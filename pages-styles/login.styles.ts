import { Theme } from "@mui/material/styles";
import { createStyles, makeStyles } from "@mui/styles";
import { Colors } from "../src/theme";
import ColorUtils, { commonRoot } from "../utils/ColorUtils";

const useStyles = makeStyles((theme: Theme) => createStyles({
    root: {
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
        width: '100%',
        justifyContent: 'center',
        ...commonRoot
    },
    subtitle: {
        marginTop: '16px',
        marginBottom: '32px'
    },
    button: { 
        marginTop: '32px',
        padding: 16,
        maxWidth: 300
    },
    textfield: {
        textAlign: 'center',
        maxWidth: 300
    }
}))

export default useStyles

