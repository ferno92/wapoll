import { createStyles, makeStyles } from "@mui/styles";
import { commonRoot } from "../utils/ColorUtils";

const useStyles = makeStyles(() => createStyles({
    root: {
        display: 'flex',
        flexDirection: 'column',
        textAlign: 'center',
        ...commonRoot
    },
    text: {
        margin: 16
    },
    boldText: {
        fontWeight: 600
    },
    name: {
        textAlign: 'left',
        paddingLeft: 16,
        paddingBottom: 4
    },
    button: {
        margin: '32px auto',
        padding: 16
    }
}))

export default useStyles