import { createStyles, makeStyles } from "@mui/styles";
import { commonRoot } from "../utils/ColorUtils";

const useStyles = makeStyles(() => createStyles({
    root: {
        display: 'flex',
        flexDirection: 'column',
        ...commonRoot
    },
    title: {
        marginBottom: 16,
        textAlign: 'center',
        marginTop: 40
    },
    title2: {
        marginTop: 32,
        marginBottom: 16
    },
    textField: {
        margin: '8px 0'
    },
    button: {
        margin: '32px 0',
        padding: 16
    },
    close: {
        position: 'absolute',
        top: 16,
        right: 16
    }
}))

export default useStyles