import { createStyles, makeStyles } from "@mui/styles";

const useStyles = makeStyles(() => createStyles({
    root: {
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
        justifyContent: 'center',
        height: "100%",
        padding: 16,
        textAlign: 'center'
    },
    progress: {
        alignSelf: 'center',
        top: '45%',
        position: 'absolute'
    },
    text: {
        margin: 8
    },
    boldText: {
        fontWeight: 600,
        display: 'inline-block'
    },
    buttonContainer: {
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'space-around',
        width: '100%',
        marginTop: 32,
        flexWrap: 'wrap',
        gap: 16
    },
    button: {
        padding: 16,
        flexGrow: 1
    }
}))

export default useStyles