import { Theme } from "@mui/material/styles";
import { createStyles, makeStyles } from "@mui/styles";

const useStyles = makeStyles((theme: Theme) => createStyles({
    title: {
        color: theme.palette.primary.main,
        backgroundColor: theme.palette.background.default
    },
    toggleIcon: {
        fill: theme.palette.text.primary
    }
}))

export default useStyles