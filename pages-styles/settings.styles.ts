import { createStyles, makeStyles } from "@mui/styles";
import { commonRoot } from "../utils/ColorUtils";

const useStyles = makeStyles(() => createStyles({
    root: {
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
        justifyContent: 'flex-start',
        textAlign: 'center',
        ...commonRoot
    },
    title: {
        marginBottom: 48,
        marginTop: 40
    },
    textfield: {
        marginTop: 32,
        marginBottom: 32,
        maxWidth: 300
    },
    button: {
        margin: '32px auto',
        padding: '16px 48px',
        maxWidth: 300
    },
    close: {
        position: 'absolute',
        top: 16,
        right: 16
    }
}))

export default useStyles