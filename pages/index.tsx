import { Accordion, AccordionDetails, AccordionSummary, Alert, Button, Chip, CircularProgress, IconButton, Snackbar, Typography } from '@mui/material'
import type { NextPage } from 'next'
import { useRouter } from 'next/dist/client/router'
import React, { useCallback, useEffect, useState } from 'react'
import Text from '../components/Text'
import firebase from '../firebase/clientApp'
import useStyles from '../pages-styles/index.styles'
import { useCollection } from "react-firebase-hooks/firestore"
import { Poll, User } from '../models/Poll'
import { Delete, Edit, ExpandMore, ListAlt, Settings, Share } from '@mui/icons-material'
import AlertDialog, { AlertDialogProps } from '../components/AlertDialog'
import Toast, { ToastDuration } from '../components/Toast'
import clsx from 'clsx'
import TextUtils from '../utils/TextUtils'

interface ToastInfo {
  message: string
  duration: ToastDuration
}

const Home: NextPage = () => {
  const [user, setUser] = useState<User>()
  const [polls, setPolls] = useState<Poll[]>([])
  const styles = useStyles()
  const router = useRouter()
  const [pollsCollection] = useCollection(
    firebase.firestore().collection('polls'),
    {}
  )
  const [pollDeleteId, setPollDeleteId] = useState<string>()
  const [pollEditId, setPollEditId] = useState<string>()
  const [newPoll, setNewPoll] = useState<Poll>()
  const [toastInfo, setToastInfo] = useState<ToastInfo>()
  const [alertDialogProps, setAlertDialogProps] = useState<AlertDialogProps>()

  const comparePolls = (a: Poll, b: Poll): number => {
    if ( a.timestamp < b.timestamp ){
      return -1
    }
    if ( a.timestamp > b.timestamp ){
      return 1
    }
    return 0
  }

  const getMergedPolls = (mine: Poll[], voted: Poll[]): Poll[] => {
    var mergedPolls = [...mine]
    voted.forEach(poll => {
      const copy = mine.find(item => item.id == poll.id)
      if (copy == undefined) {
        mergedPolls.push(poll)
      }
    })
    mergedPolls.sort(comparePolls).reverse()
    return mergedPolls
  }

  const fetchPolls = useCallback(async (id: String) => {
    const query = await (pollsCollection?.query.where("owner", "==", id).get())
    const queryVoters = await (pollsCollection?.query.where("voters", "array-contains", id).get())

    const items = query?.docs.map((doc) => {
      console.log("item -> ", doc.data())
      const poll = doc.data() as Poll
      poll.id = doc.id
      return poll
    }) || []

    const itemsVoters = queryVoters?.docs.map((doc) => {
      console.log("itemsVoters -> ", doc.data())
      const poll = doc.data() as Poll
      poll.id = doc.id
      return poll
    }) || []

    const mergedPolls = getMergedPolls(items, itemsVoters)
    console.log("items for ", id, mergedPolls, pollsCollection)
    const { editId } = router.query
    if (editId) {
      const poll = mergedPolls.find(item => item.id == editId)
      setNewPoll(poll)
      setToastInfo({ message: `Sondaggio "${poll?.question}" creato con successo!`, duration: ToastDuration.long })
    }

    setPolls(mergedPolls)

  }, [pollsCollection])

  const onClick = useCallback(() => {
    router.push("/edit-poll")
  }, [])

  const onDelete = useCallback((id: string | undefined) => {
    if (id) {
      const db = firebase.firestore()
      const ref = db.collection('polls').doc(id)
      ref.delete()
      setPollDeleteId(undefined)
    }
  }, [])

  const onShare = useCallback((id: string) => {
    const poll = polls?.filter(item => item.id == id)[0]
    const text = TextUtils.getSharedText(poll)
    if (navigator.share != undefined) {
      navigator.share({
        title: 'WaPoll Share',
        text: text
      }).then(() => {
        console.log("it works!")
      }).catch(console.error)
    } else {
      navigator.clipboard.writeText(text)
      setToastInfo({ message: 'Sondaggio copiato negli appunti', duration: ToastDuration.short })
    }
  }, [polls])

  const onResult = useCallback((pollId: string) => {
    router.push({
      pathname: '/result/',
      query: { id: pollId }
    })
  }, [router])

  const onSettings = useCallback(() => {
    router.push('/settings')
  }, [])

  const onAskEdit = useCallback((id: string) => {
    setPollEditId(id)
  }, [])

  const onEdit = useCallback((id: string) => {
    setPollEditId(undefined)
    router.push({
      pathname: '/edit-poll',
      query: { id }
    })
  }, [router])

  const getDate = useCallback((seconds: number | undefined): string | undefined => {
    if (seconds != undefined) {
      var t = new Date(1970, 0, 1) // Epoch
      t.setSeconds(seconds)
      return t.toLocaleDateString()
    } else {
      return undefined
    }
  }, [])

  const showDate = useCallback((seconds: number | undefined, previousSeconds: number | undefined): boolean => {
    if (seconds != undefined) {
      if(previousSeconds != undefined) {
        var date = new Date(1970, 0, 1) // Epoch
        date.setSeconds(seconds)
        var prevDate = new Date(1970, 0, 1) // Epoch
        prevDate.setSeconds(previousSeconds)
        const sameDay = (date.getDate() === prevDate.getDate() 
        && date.getMonth() === prevDate.getMonth()
        && date.getFullYear() === prevDate.getFullYear())
        return !sameDay
      } else {
        return true
      }
    } else {
      return false
    }
  }, [])

  useEffect(() => {
    if (window) {
      const name = window.localStorage.getItem('name')
      const id = window.localStorage.getItem('id')
      if (name && id) {
        setUser({ id, name })
        fetchPolls(id)
      } else {
        router.push("/login")
      }
    }
  }, [fetchPolls])

  useEffect(() => {
    if (pollDeleteId) {
      setAlertDialogProps({
        open: pollDeleteId != undefined,
        title: 'Elimina',
        message: 'Sei sicuro di voler eliminare il sondaggio?',
        buttons: [
          {
            text: 'Sì',
            onClick: () => onDelete(pollDeleteId)
          },
          {
            text: 'No',
            onClick: () => setPollDeleteId(undefined)
          }
        ]
      })
    } else {
      setAlertDialogProps(undefined)
    }
  }, [pollDeleteId])

  useEffect(() => {
    if (pollEditId) {
      setAlertDialogProps({
        open: pollEditId != undefined,
        title: 'Modifica',
        message: 'Modificando questo sondaggio ne creerai uno nuovo partendo da quello attuale. Vuoi procedere?',
        buttons: [
          {
            text: 'Sì',
            onClick: () => onEdit(pollEditId)
          },
          {
            text: 'No',
            onClick: () => setPollEditId(undefined)
          }
        ]
      })
    } else {
      setAlertDialogProps(undefined)
    }
  }, [pollEditId])

  return (
    <div className={styles.root}>
      {/* <ThemeToggle /> */}
      {user &&
        <IconButton className={styles.settings} onClick={onSettings}>
          <Settings />
        </IconButton>
      }
      {user &&
        <>
          <Text variant='h3' className={styles.title}>Ciao <Typography variant='h2' className={styles.name} color='primary'>{user.name}</Typography></Text>
          <Button variant='contained' onClick={onClick} className={styles.button}>Crea sondaggio</Button>
          <div className={clsx(styles.accordionContainer, polls.length <= 0 ? styles.accordionContainerCentered : '')}>
            {polls.map((poll, pIndex) =>
              <div key={poll.id} className={styles.itemWithDate}>
                {showDate(poll.timestamp?.seconds, (pIndex != 0 ? polls[pIndex - 1].timestamp?.seconds : undefined)) && 
                <Chip label={getDate(poll.timestamp?.seconds)} className={styles.chipDate} /> 
              }
                <div className={styles.item}>
                <Accordion className={styles.accordion} defaultExpanded={newPoll?.id == poll.id}>
                  <AccordionSummary expandIcon={<ExpandMore className={styles.accordionIcon} />}>
                    <Text>{poll.question}</Text>
                  </AccordionSummary>
                  <AccordionDetails>
                    {poll.answers.map(answer =>
                      <Typography
                        key={answer.id}
                        className={styles.user}>
                        {`• ${answer.text}${(answer.votedBy || []).length > 0 ? `   (${answer.votedBy?.length} voti)` : ''}`}
                      </Typography>
                    )}
                    <div className={styles.iconContainer}>
                      <IconButton
                        className={styles.iconButton}
                        color='primary'
                        onClick={() => onShare(poll.id)}>
                        <Share />
                      </IconButton>
                      <IconButton
                        className={styles.iconButton}
                        color='secondary'
                        onClick={() => onResult(poll.id)}>
                        <ListAlt />
                      </IconButton>
                      <IconButton
                        className={styles.iconButton}
                        color='warning'
                        onClick={() => onAskEdit(poll.id)}>
                        <Edit />
                      </IconButton>
                      {poll.owner == user.id && (
                        <IconButton className={styles.iconButton} color='error' onClick={() => setPollDeleteId(poll.id)}>
                          <Delete />
                        </IconButton>
                      )}
                    </div>
                    {poll.timestamp && <Typography className={styles.date} variant='caption'>{poll.timestamp.toDate().toLocaleString()}</Typography>}
                  </AccordionDetails>
                </Accordion>
              </div>
              </div>
            )}
            {polls.length == 0 && (
              <div className={styles.empty}>
                <img className={styles.emptyImage} src='/images/to-do-list.png' />
                <Typography>Non ci sono sondaggi qui, creane uno!</Typography>
              </div>
            )}
          </div>
        </>
      }
      {user === undefined &&
        <CircularProgress className={styles.progress} />
      }
      <AlertDialog
        open={alertDialogProps?.open == true}
        title={alertDialogProps?.title}
        message={alertDialogProps?.message}
        buttons={alertDialogProps?.buttons || []}
      />

      <Toast
        duration={toastInfo?.duration}
        message={toastInfo?.message}
        severity='success'
        onClose={() => setToastInfo(undefined)} />
    </div>
  )
}

export default Home
