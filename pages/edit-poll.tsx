import { Button, IconButton, TextField, Typography } from "@mui/material"
import { NextPage } from "next"
import { nanoid } from 'nanoid'
import React, { useCallback, useEffect, useState } from "react"
import firebase from '../firebase/clientApp'
import useStyles from "../pages-styles/edit-poll.styles"
import { Answer, Poll } from "../models/Poll"
import { useRouter } from "next/router"
import { Close } from "@mui/icons-material"
import { useCollection } from "react-firebase-hooks/firestore"


const EditPoll: NextPage = () => {
    const emptyAnswer = { id: nanoid(5), text: "", votedBy: [] }
    const [question, setQuestion] = useState("")
    const [answers, setAnswers] = useState<Answer[]>([emptyAnswer])
    const styles = useStyles()
    const router = useRouter()
    const [pollsCollection, pollsLoading, pollsError] = useCollection(
        firebase.firestore().collection('polls'),
        {}
    )
    const { id:lastId } = router.query

    const onChangeQuestion = useCallback((text: string) => {
        setQuestion(text)
    }, [])

    const onChangeAnswer = useCallback((text: string, index: number) => {
        var newAnswers = [...answers]
        if (index == newAnswers.length - 1) {
            //add textfield
            newAnswers = newAnswers.concat({ id: nanoid(5), text: "", votedBy: [] })
        }
        newAnswers[index] = { id: newAnswers[index].id, text: text, votedBy: newAnswers[index].votedBy }
        setAnswers(newAnswers)
    }, [answers])

    const onBlur = useCallback((index: number) => {
        var newAnswers = [...answers]
        if (index !== newAnswers.length - 1 && newAnswers[index].text.trim() === "") {
            // remove textfield
            newAnswers.splice(index, 1)
            setAnswers(newAnswers)
        }
    }, [answers])

    const onSave = useCallback(async () => {
        const owner = window.localStorage.getItem('id')
        if(owner) {
            const db = firebase.firestore()
            const filteredAnswers: Answer[] = answers.filter(item => item.text.trim() != "")
            const editId = nanoid(5)
            const poll: Poll = {
                id: editId,
                question,
                owner: owner,
                answers: filteredAnswers,
                timestamp: firebase.firestore.Timestamp.now(),
                voters: []
            }
            await db.collection("polls").doc(editId).set(poll)
    
            router.push({
                pathname: '/',
                query: { editId }
            })
        }
    }, [question, answers])

    const onClose = useCallback(()=> {
        router.push('/')
    }, [router])

    const fetchQuestion = useCallback(async (id: string) => {
        console.log("fetchQuestion", pollsCollection, pollsLoading, pollsError, id)
        const polls = (await pollsCollection?.query.where(firebase.firestore.FieldPath.documentId(), "==", id).get())?.docs.map(doc => doc.data() as Poll)
        if (polls && polls.length > 0) {
            setQuestion(polls[0].question)
            setAnswers(polls[0].answers.concat([{...emptyAnswer}]))
        } else {
            //TODO an error occurred, redirect? show message?
            console.log("fetchQuestion error")
        }
    }, [pollsCollection, pollsLoading, answers])

    useEffect(() => {
        if (!pollsLoading && lastId != undefined) {
            fetchQuestion(lastId.toString())
        }
    }, [lastId, pollsLoading])

    return (
        <div className={styles.root}>
        <IconButton className={styles.close} onClick={onClose}>
            <Close />
        </IconButton>
            <Typography className={styles.title} variant="h3" color='primary'>Nuovo Sondaggio</Typography>
            <TextField
                value={question}
                placeholder="Inserisci la tua domanda.."
                multiline
                fullWidth
                onChange={(event) => onChangeQuestion(event.target.value)}
                variant='outlined'
                className={styles.textField}
                autoFocus
            />
            <Typography className={styles.title2} variant='h5'>Risposte:</Typography>
            {answers.map((answer, index) =>
                <TextField
                    key={answer.id}
                    value={answer.text} placeholder="Inserisci una risposta.."
                    fullWidth
                    onChange={(event) => onChangeAnswer(event.target.value, index)}
                    onBlur={() => onBlur(index)}
                    variant='outlined'
                    className={styles.textField} />
            )
            }
            <Button
                onClick={onSave}
                disabled={answers.filter(item => item.text.trim() !== "").length == 0}
                className={styles.button}
                variant='contained'
            >Salva</Button>
        </div>
    )
}

export default EditPoll