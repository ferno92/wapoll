import firebase from '../../firebase/clientApp'
import { NextPage } from "next"
import { useRouter } from "next/router"
import React, { useCallback, useEffect, useState } from "react"
import { Answer, Poll, User, UserWithTime } from '../../models/Poll'
import { useCollection } from "react-firebase-hooks/firestore"
import useStyles from '../../pages-styles/vote.styles'
import { Button, CircularProgress, Typography } from '@mui/material'

const VotePage: NextPage = () => {
    const router = useRouter()
    const { id, a } = router.query
    const [pollsCollection, pollsLoading, pollsError] = useCollection(
        firebase.firestore().collection('polls'),
        {}
    )
    const [voted, setVoted] = useState(false)
    const [poll, setPoll] = useState<Poll>()
    const [greeting, setGreeting] = useState<string>()
    const [previousAnswer, setPreviousAnswer] = useState<Answer>()
    const styles = useStyles()

    const fetchQuestion = useCallback(async (user: UserWithTime) => {
        console.log("fetchQuestion", pollsCollection, pollsLoading, pollsError, id)
        const polls = (await pollsCollection?.query.where(firebase.firestore.FieldPath.documentId(), "==", id).get())?.docs.map(doc => doc.data() as Poll)
        if (polls && polls.length > 0) {
            setPoll(polls[0])
            const answerVoted = polls[0]?.answers.find(item => item.id == a)
            const alreadyVoted = answerVoted?.votedBy?.find(item => item.id == user.id) != null
            if (!alreadyVoted) {
                vote(polls[0], user)
            } else {
                setVoted(true)
            }
        } else {
            //TODO an error occurred, redirect? show message?
            console.log("fetchQuestion error")
        }
    }, [id, pollsCollection, pollsLoading])

    const vote = useCallback(async (poll: Poll, user: UserWithTime) => {
        const db = firebase.firestore()
        var answers = [...poll.answers]
        var voters = poll.voters ? [...poll.voters] : []
        answers.forEach(answer => {
            if (answer.id == a){
                if(answer.votedBy?.find(item => item.id == user.id) == undefined) { // no duplicates
                    if (answer.votedBy) {
                        answer.votedBy = answer.votedBy.concat(user)
                    } else {
                        answer.votedBy = [user]
                    }
                    voters.concat(user.id)
                }
            } else {
                const index = answer.votedBy?.findIndex(item => item.id == user.id)
                if(index != undefined && index != -1) {
                    setPreviousAnswer(answer)
                    answer.votedBy?.splice(index, 1)
                }
            }
                
        })
        await db.collection('polls').doc(`${id}`).update({
            answers: answers
        }).then(() => {
            console.log("success", answers)
            setVoted(true)
        }).catch(e => console.log("error update", e))
    }, [id, a])

    const onClickHome = useCallback(() => {
        router.push('/')
    }, [])

    const onClickResult = useCallback(()=> {
        router.push({
            pathname: '/result/',
            query: { id }
        })
    }, [id, router])

    const goToLogin = useCallback(() => {
        router.push({
            pathname: '/login',
            query: { id, a }
        })
    }, [id, a, router])

    useEffect(() => {
        if (window) {
            const name = window.localStorage.getItem('name')
            const userId = window.localStorage.getItem('id')
            if (name && userId) {
                if (!pollsLoading) {
                    const greetingsList = ['Stupendo', 'Splendido', 'Magnifico', 'Fantastico', 'Meraviglioso', 'Ottimo', 'Complimenti']
                    const random = Math.floor(Math.random() * greetingsList.length)
                    setGreeting(greetingsList[random])
                    fetchQuestion({ id: userId, name, timestamp: firebase.firestore.Timestamp.now() })
                }
            } else if (id && a) {
                goToLogin()
            }
        }
    }, [pollsLoading, id, a])

    const answerVoted = poll?.answers.find(item => item.id == a)
    const answerVotedBy = answerVoted?.votedBy?.filter(item => item.id != window?.localStorage.getItem('id')).length || 0
    return (
        <div className={styles.root}>
            {voted &&
                <>
                    <Typography variant='h3' color='primary' sx={{ marginBottom: '32px' }}>{greeting}!</Typography>
                    <Typography className={styles.text}>Alla domanda "<Typography className={styles.boldText} color='secondary'>{poll?.question}</Typography>"</Typography>
                    <Typography className={styles.text}>Hai risposto
                        "<Typography className={styles.boldText} color='secondary'>{answerVoted?.text}</Typography>"
                        {answerVotedBy > 0 && ` come altre ${answerVotedBy} persone`}
                    </Typography>
                    {previousAnswer && 
                        <Typography className={styles.text}>(Precedentemente avevi risposto "<Typography className={styles.boldText} color='secondary'>{previousAnswer.text}</Typography>", il tuo voto è stato sostituito)</Typography>
                    }
                    <div className={styles.buttonContainer}>
                        <Button variant='contained' className={styles.button} color='secondary' onClick={onClickResult}>Vedi risultati</Button>
                        <Button variant='contained' className={styles.button} onClick={onClickHome}>Torna alla homepage</Button>
                    </div>
                </>
            }
            {!voted &&
                <CircularProgress className={styles.progress} />}
        </div>
    )
}

export default VotePage