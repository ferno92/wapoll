import { NextPage } from "next";
import Link from "next/link";
import React from "react";
import useStyles from "../pages-styles/first-post.styles";

const FirstPost: NextPage = () => {

    
    const styles = useStyles()
    return (
        <>
            <h1 className={styles.title}>First post</h1>
            <h2>
                <Link href="/">
                    <a>Back to home</a>
                </Link>
            </h2>
        </>
    )
}

export default FirstPost