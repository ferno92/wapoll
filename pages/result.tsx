import { ExpandMore } from "@mui/icons-material";
import { Accordion, AccordionDetails, AccordionSummary, Button, Typography } from "@mui/material";
import clsx from "clsx";
import { NextPage } from "next";
import { useRouter } from "next/router";
import React, { useCallback, useEffect, useState } from "react";
import { useCollection } from "react-firebase-hooks/firestore";
import firebase from '../firebase/clientApp'
import { Answer, Poll } from "../models/Poll";
import useStyles from "../pages-styles/result.styles";

const ResultPage: NextPage = () => {
    const router = useRouter()
    const { id } = router.query
    const [pollsCollection, pollsLoading, pollsError] = useCollection(
        firebase.firestore().collection('polls'),
        {}
    )
    const [poll, setPoll] = useState<Poll>()
    const styles = useStyles()

    const fetchQuestion = useCallback(async () => {
        const polls = (await pollsCollection?.query.where(firebase.firestore.FieldPath.documentId(), "==", id).get())?.docs.map(doc => doc.data() as Poll)
        if (polls && polls.length > 0) {
            setPoll(polls[0])
        } else {
            //TODO an error occurred, redirect? show message?
            console.log("fetchQuestion error")
        }
    }, [id, pollsCollection])

    const getAnswerColor = useCallback((answer: Answer): string => {
        const maxVotes = Math.max.apply(Math, poll?.answers.map(o => o.votedBy?.length || 0) || [])
        return answer.votedBy?.length == maxVotes ? 'primary' : 'default'
    }, [poll])

    const onClickHome = useCallback(() => {
        router.push('/')
    }, [])

    const sameUser = useCallback((id: string): boolean => {
        if(window) {
            return window.localStorage.getItem('id') == id
        } else {
            return false
        }
    }, [])

    useEffect(() => {
        if (window) {
            if (!pollsLoading) {
                fetchQuestion()
            }
        }
    }, [pollsLoading])

    return <div className={styles.root}>
        <Typography className={styles.text} variant='h3' color='primary'>Risultati</Typography>
        {poll && (
            <>
                <Typography className={styles.text}>{poll.question}</Typography>
                {poll.answers.map(answer =>
                    <Accordion
                        key={answer.id}
                        disabled={answer.votedBy == undefined || answer.votedBy?.length == 0}
                        defaultExpanded={answer.votedBy && answer.votedBy.length > 0}>
                        <AccordionSummary expandIcon={<ExpandMore />}>
                            <Typography
                                color={/*getAnswerColor(answer)*/'default'}>
                                {`${answer.text} (${answer.votedBy?.length || 0} voti)`}
                            </Typography>
                        </AccordionSummary>
                        <AccordionDetails>
                            {answer.votedBy?.map(user =>
                                <Typography key={user.id} className={clsx(styles.name, sameUser(user.id) ? styles.boldText : '')}>{user.name}</Typography>
                            )}
                        </AccordionDetails>
                    </Accordion>
                )}
                <Button className={styles.button} variant='contained' onClick={onClickHome}>Torna alla homepage</Button>
            </>
        )}
    </div>
}

export default ResultPage