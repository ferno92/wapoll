import { Close } from "@mui/icons-material";
import { Alert, Button, FormControlLabel, IconButton, Snackbar, Switch, TextField, Typography } from "@mui/material";
import { NextPage } from "next";
import { useRouter } from "next/router";
import React, { useCallback, useContext, useEffect, useState } from "react";
import useStyles from "../pages-styles/settings.styles";
import { ThemeContext } from "../pages/_app"

const SettingsPage: NextPage = () => {
    const { mode, setMode } = useContext(ThemeContext)
    const toggleDarkMode = () => {
        setMode(mode !== 'dark' ? 'dark' : 'light')
    }
    const [name, setName] = useState<string>()
    const [localName, setLocalName] = useState<string>()
    const [open, setOpen] = useState(false)
    const router = useRouter()
    const styles = useStyles()

    const onChange = useCallback((text: string) => {
        setName(text)
    }, [])

    const onSave = useCallback(() => {
        if (name) {
            window.localStorage.setItem('name', name)
            setLocalName(name)
            setOpen(true)
        }
    }, [name])

    const onClose = useCallback(() => {
        router.push('/')
    }, [router])

    useEffect(() => {
        if (window) {
            const local = window.localStorage.getItem('name') || undefined
            setLocalName(local)
            setName(local)
        }
    }, [])

    return (
        <div className={styles.root}>
            <IconButton className={styles.close} onClick={onClose}>
                <Close />
            </IconButton>
            <Typography variant='h3' color='primary' className={styles.title}>Impostazioni</Typography>
            <FormControlLabel
                control={
                    <Switch
                        checked={mode == 'dark'}
                        onChange={toggleDarkMode}
                    />
                }
                label='Dark Mode'
            />
            <TextField
                fullWidth
                value={name}
                placeholder="Scrivi il tuo nome.."
                onChange={(event) => onChange(event.target.value)}
                variant='outlined'
                className={styles.textfield} />
            <Button
                fullWidth
                variant='contained'
                color='primary'
                disabled={name == localName || localName == undefined || name == undefined || name?.trim() == ''}
                className={styles.button}
                onClick={onSave}>Salva</Button>

            <Snackbar open={open} autoHideDuration={3000} onClose={() => setOpen(false)}>
                <Alert variant='filled' onClose={() => setOpen(false)} severity="success" sx={{ width: '100%' }}>
                    Nome salvato!
                </Alert>
            </Snackbar>
        </div>
    )
}

export default SettingsPage