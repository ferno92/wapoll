import { Button, TextField } from "@mui/material";
import { customAlphabet } from "nanoid";
import { NextPage } from "next";
import { useRouter } from "next/dist/client/router";
import React, { useCallback, useEffect, useState } from "react";
import Text from "../components/Text";
import useStyles from "../pages-styles/login.styles";


const Login: NextPage = () => {
    const [name, setName] = useState<string | null>()
    const styles = useStyles()
    const router = useRouter()

    const onChange = useCallback((text: string) => {
        setName(text)
    }, [])

    const onConfirm = useCallback(() => {
        const values = name?.split('#')
        if (values && values.length > 1) {
            window.localStorage.setItem('id', values[1])
        } else {
            const nanoid = customAlphabet('0123456789', 5)
            window.localStorage.setItem('id', nanoid())
        }
        window.localStorage.setItem('name', values ? values[0] : '')
        const { id, a } = router.query
        console.log("push", router.query)
        if (id && a) {
            router.push(`/vote/${id}?a=${a}`)
        } else {
            router.push('/')
        }
    }, [name])

    useEffect(() => {
        if (window) {
            setName(window.localStorage.getItem('name'))
        }
    }, [])

    return (
        <div className={styles.root}>
            <Text variant="h2" children="Ciao" />
            <Text variant="body1" className={styles.subtitle}>Per cominciare inserisci il tuo nome</Text>
            <TextField
                fullWidth
                value={name}
                placeholder="Scrivi il tuo nome.."
                onChange={(event) => onChange(event.target.value)}
                variant='outlined'
                className={styles.textfield} />
            <Button
                fullWidth
                onClick={onConfirm}
                variant='contained'
                className={styles.button}
                disabled={name == null || name.trim() == ''}>Continua</Button>
        </div>
    )
}

export default Login