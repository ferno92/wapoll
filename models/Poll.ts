import firebase from '../firebase/clientApp'

export interface User {
    id: string
    name: string
}

export interface UserWithTime extends User {
    timestamp: firebase.firestore.Timestamp
}

export interface Answer {
    id: string
    text: string
    votedBy: UserWithTime[] | undefined
}

export interface Poll {
    id: string
    question: string
    answers: Answer[]
    owner: string
    timestamp: firebase.firestore.Timestamp
    voters: string[] | undefined
}