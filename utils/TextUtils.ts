import { Poll } from "../models/Poll"

class TextUtils {
    static getSharedText(poll: Poll) {
        return `*${poll.question}* \n\n${poll.answers.map(answer => `${answer.text}\nhttps://${window.location.host}/vote/${poll.id}?a=${answer.id}\n\n`).join('')}*Risultati* -> ${window.location.host}/result?id=${poll.id}`
    }
}

export default TextUtils