import { Brightness6 } from "@mui/icons-material"
import { IconButton } from "@mui/material"
import React, { useContext } from "react"
import { ThemeContext } from "../pages/_app"
import useStyles from "./ThemeToggle.styles"

const ThemeToggle = () => {

  const styles = useStyles()

  const { mode, setMode } = useContext(ThemeContext)
  const toggleDarkMode = () => {
    setMode(mode !== 'dark' ? 'dark' : 'light')
  }
  return (
    <IconButton className={styles.button} onClick={toggleDarkMode} >
      <Brightness6 className={styles.toggleIcon} />
    </IconButton>
  )
}

export default ThemeToggle