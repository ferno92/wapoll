import React, { Component, useEffect } from "react";

interface GoogleAdProps {
    slot: number,
    className: string | undefined
}

const GoogleAd: React.FC<GoogleAdProps> = (props: GoogleAdProps) => {
    let { slot, className } = props
    var googleInit: NodeJS.Timeout | null = null
    var timeout = 200
    useEffect(()=> {
        googleInit = setTimeout(() => {
            if (typeof window !== 'undefined')
              (window.adsbygoogle = window.adsbygoogle || []).push({})
          }, timeout)
          return () => {
              if(googleInit != null) {
                clearTimeout(googleInit)
              }
          }
    }, [])

    return (
        <div className={className}>
          <ins
            className="adsbygoogle"
            style={{ display: 'block' }}
            data-ad-client={process.env.NEXT_PUBLIC_ADS}
            data-ad-slot={slot}
            data-ad-format="auto"
            data-full-width-responsive="true"
          ></ins>
        </div>
    )
}
  
  export default GoogleAd