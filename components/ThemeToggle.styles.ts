import { createStyles, makeStyles } from "@mui/styles";

const useStyles = makeStyles(() => createStyles({
    toggleIcon: {
        //fill: theme.palette.text.primary,
    },
    button: {
        position: 'absolute',
        top: 16,
        right: 16
    }
}))

export default useStyles