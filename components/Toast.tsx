import { Alert, AlertColor, Snackbar } from "@mui/material"
import React from "react"

export enum ToastDuration {
    short = 3000,
    long = 6000
} 

interface ToastProps {
    message: string | undefined
    duration: ToastDuration | undefined
    severity: AlertColor | undefined
    onClose: () => void
}

const Toast: React.FC<ToastProps> = (props: ToastProps) => {
    const {duration, message, onClose, severity = 'success'} = props
    return (
      <Snackbar open={message != undefined} autoHideDuration={duration} onClose={onClose}>
      <Alert variant='filled' onClose={onClose} severity={severity} sx={{ width: '100%' }}>{message}</Alert>
    </Snackbar>
    )
}

export default Toast