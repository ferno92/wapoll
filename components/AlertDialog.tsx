import { Button, Dialog, DialogActions, DialogContent, DialogContentText, DialogTitle } from "@mui/material"
import React from "react"

interface AlertDialogButton {
    text: string
    onClick: () => void
}

export interface AlertDialogProps {
    open: boolean
    title: string | undefined
    message: string | undefined
    buttons: AlertDialogButton[]
}

const AlertDialog: React.FC<AlertDialogProps> = (props: AlertDialogProps) => {
    return (
        <Dialog open={props.open}>
            <DialogTitle>{props.title}</DialogTitle>
            <DialogContent>
                <DialogContentText>{props.message}</DialogContentText>
            </DialogContent>
            <DialogActions>
            {props.buttons.map((button, index) =>
                <Button id={`index-${index}`} onClick={button.onClick}>{button.text}</Button>
                )}
            </DialogActions>
        </Dialog>
    )
}

export default AlertDialog