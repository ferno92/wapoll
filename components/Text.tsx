import { Typography, TypographyProps } from "@mui/material"
import { Variant } from "@mui/material/styles/createTypography"
import clsx from "clsx"
import useStyles from "./Text.styles"

interface TextProps {
    children: React.ReactNode
    props?: TypographyProps,
    variant?: Variant,
    className?: string
}

const Text = (props: TextProps) => {
    const styles = useStyles()
    return (
        <Typography className={clsx(styles.text, props.className)} {...props.props} variant={props.variant}>{props.children}</Typography>
    )
}

export default Text